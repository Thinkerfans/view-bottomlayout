package com.example.bottomlayout_style_example;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class BottomTabsActivity extends FragmentActivity {

	private FragmentTabHost mTabHost;

	private static final int TOTAL_ITEMS = 4;

	private static  final int mItemsName[] = { 
			R.string.tab_first, 
			R.string.tab_second,
			R.string.tab_third, 
			R.string.tab_fourth, };
	
	private static final int mItemsView[] = { 
			R.drawable.first_tab_drawable,
			R.drawable.second_tab_drawable,
			R.drawable.third_tab_drawable,
			R.drawable.fourth_tab_drawable, 
			};

	private static final Class<?> mItemsFragment[] = { 
			FirstFragment.class,
			SecondFragment.class, 
			ThirdFragment.class, 
			FourFragment.class };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bottom_tabhost_layout);

		initTabHost();
		initTabs();

	}

	private void initTabs() {
		for (int i = 0; i < TOTAL_ITEMS; i++) {
			TabSpec tabSpec = mTabHost.newTabSpec(getString(mItemsName[i])).setIndicator(
					getTabItemView(i));
			mTabHost.addTab(tabSpec, mItemsFragment[i], null);
			mTabHost.getTabWidget().getChildAt(i)
					.setBackgroundResource(R.drawable.home_btn_bg);
		}
	}

	private View getTabItemView(int index) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabhost_item_layout, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
		imageView.setImageResource(mItemsView[index]);
		TextView textView = (TextView) view.findViewById(R.id.textview);
		textView.setText(mItemsName[index]);
		return view;
	}

	private void initTabHost() {
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

	}
}
