package com.example.bottomlayout_style_example;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FirstFragment extends Fragment {

	private Activity mActivity;
	private View mView;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (null != mView) {
			ViewGroup parent = (ViewGroup) mView.getParent();
			parent.removeView(mView);
			return mView;
		}

		mView = inflater.inflate(R.layout.first_fragment_layout, null);
		return mView;

	}

}
